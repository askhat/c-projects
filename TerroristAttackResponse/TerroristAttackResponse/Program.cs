﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerroristAttackResponse
{
    class Program
    {
        public static double Probability(double a, double b1, double b2, double x1, double x2)
        {
            double res = Math.Abs((a + b1 * x1 + b2 * x2) * a - b1 * a - b2 * a);
            return res;
        }
        static void Main(string[] args)
        {
            //Showing first JSOn
            //Parser p = new Parser();
            //List<TerrorAttack> allAttacks = p.LoadJSON();
            //foreach(var cur in allAttacks)
            //{
            //    Console.WriteLine(cur.City + " " + cur.Date);
            //}
            Parser p = new Parser();
            List<TerrorCountry> list = p.LoadSecond();
            foreach(var cur in list)
            {
                Console.WriteLine(cur.Country + " " + Probability(cur.A, cur.B1, cur.B2, cur.X1, cur.X2));
            }

            Console.ReadLine();            
        }
    }
}
