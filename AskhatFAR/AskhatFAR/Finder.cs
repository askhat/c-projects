﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace AskhatFAR {
    public static class Finder {
        private static bool checker = false;
        public static void Searcher(string sDir, string pattern) {
            foreach (string dir in Directory.GetDirectories(sDir)) {   
                try {
                    if (!checker) {
                        foreach (string file in Directory.GetFiles(dir, "*.cpp")) {
                            string txtFile = Path.GetFileName(file);
                            FileInfo ff = new FileInfo(dir + "\\" + txtFile);
                            //using REGEX
                            /*foreach (var line in File.ReadLines(ff.FullName).Where(line => regex.Match(line).Success))  {
                                File.AppendAllText("file to write to", line + Environment.NewLine);
                            }*/
                            //using contains
                            string[] lines = File.ReadAllLines(ff.FullName);
                            foreach (string curLine in lines)
                            {
                                if (curLine.Contains(pattern))
                                {
                                    MessageBox.Show("FOUND! " + ff.FullName);
                                    checker = true;
                                    return;
                                }
                            }
                            //Console.WriteLine(txtFile);
                        }
                        Searcher(dir, pattern);
                    }
                   
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
