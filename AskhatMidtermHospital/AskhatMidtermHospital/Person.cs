﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskhatMidtermHospital {
    
    public enum Genders { Male, Female };
    [Serializable()]
    public class Person {
        public string  FirstName { get; set; }
        public int Age { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public Genders Gender { get; set; }
        public Person(string _firstName, string _lastName, int _age,string _phoneNumber, Genders gender) {
            FirstName = _firstName;
            Age = _age;
            LastName = _lastName;
            PhoneNumber = _phoneNumber;
            Gender = gender;
        }
    }


}

