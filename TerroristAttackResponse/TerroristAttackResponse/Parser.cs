﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace TerroristAttackResponse
{
    class Parser
    {
        //first JSON
        public List<TerrorAttack> LoadFirst()
        {
            StreamReader r = new StreamReader(@"C:\texts\first.json");
            string json = r.ReadToEnd();
            List<TerrorAttack> items = JsonConvert.DeserializeObject<List<TerrorAttack>>(json);
            return items;                                                                    
        }

        //second JSON
        public List<TerrorCountry> LoadSecond()
        {
            StreamReader r = new StreamReader(@"C:\texts\second.json");
            string json = r.ReadToEnd();
            List<TerrorCountry> items = JsonConvert.DeserializeObject<List<TerrorCountry>>(json);
            return items;
        }                                   
    }
}
