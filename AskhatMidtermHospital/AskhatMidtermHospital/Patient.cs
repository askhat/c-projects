﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskhatMidtermHospital {
    class Patient : Person {
        public string Disease { get; set; }
        public int SicknessLevel { get; set; }
        public bool HasAllergies { get; set; }
        public DateTime AcceptedDate { get; set; }
        public bool SpecialRequirements { get; set; }

        public Patient(string _firstName, string _lastName, int _age,string _phoneNumber, Genders gender, 
            int _sicknessLevel, bool _hasAllergies, DateTime _acceptedDate, bool _specialRequirements, string _disease)
            : base(_firstName, _lastName, _age, _phoneNumber, gender)
        {
            SicknessLevel = _sicknessLevel;
            HasAllergies = _hasAllergies;
            AcceptedDate = _acceptedDate;
            Disease = _disease;
            SpecialRequirements = _specialRequirements;
        }
    }
}
