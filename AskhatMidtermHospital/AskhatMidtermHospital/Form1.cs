﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AskhatMidtermHospital
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e) {

            //deserialize and add
            Model.bl.Add(new Person("Askhat", "Temir", 20, "28123124", Genders.Male));
            Model.bl.Add(new Person("Urazaliev", "Kanat", 20, "123124124", Genders.Male));
            Model.bl.Add(new Doctor("Mussinov", "Aslan", 21, "7777774", Genders.Male, "KBTU", 12, 3));
            Model.bl.Add(new Person("Kuralbai", "Bekarys", 20, "9983124", Genders.Male));
            Model.bl.Add(new Person("Aitzhan", "Askar", 20, "77721412", Genders.Male));

            dataGridView1.DataSource = Model.bl;
            dataGridView1.Refresh();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
           // Controller c = new Controller();
            //BindingList<Person> show = c.Deserialize();
            //dataGridView1.DataSource = show;
            dataGridView1.Refresh();
        }
        Controller c = new Controller();
        private void button2_Click(object sender, EventArgs e) {
            c.Ser();
        }

        private void button3_Click(object sender, EventArgs e) {
            dataGridView1.DataSource = c.des();
            dataGridView1.Refresh();

        }
    }
}
