﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskhatFAR {
    class Design {
            public static int part_size = 34 / 5;
            public static int space = 0;
            public static int cnt = 0;
            public static int width = 100;
            public static int height = 34;
            public static int block = width / 11;
            public static int x = 0, y = 0;
            public static void InitWindow() {
            Console.Title = "Askhat's FAR Manager";
            Console.CursorSize = (int)1;
            Console.SetWindowSize(width, height);
            Console.SetBufferSize(width, height);
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.ForegroundColor = ConsoleColor.White; 
            Console.SetCursorPosition(0, 0);
            Console.Write(' ');
            for (int i = 0; i < height; i++) Console.WriteLine("\n");
            Console.SetCursorPosition(0, height - 1);
            Console.BackgroundColor = ConsoleColor.Black;
            for (int i = 0; i < width; i++) {
                for (int j = height - 1; j < height; j++) {
                    Console.SetCursorPosition(i, j);
                    Console.Write(' ');
                }
            }
            Console.BackgroundColor = ConsoleColor.DarkCyan;
           
            //window dividing
            height -= 2;
            Console.ForegroundColor = ConsoleColor.Cyan;
            draw(x, y, x + 1, y + height, '║' );
            draw(x - 1 + width, y, x + width, y + height, '║');
            draw(x, y, x + width, y + 1, '═');
            draw(x, y - 1 + height, x + width, y + height, '═');
            Console.SetCursorPosition(x, y);
            Console.Write('╔');
            Console.SetCursorPosition(x + width - 1, y);
            Console.Write('╗');
            Console.SetCursorPosition(x, y + height - 1);
            Console.Write('╚');
            Console.SetCursorPosition(x + width - 1, y + height - 1);
            Console.Write('╝');
            draw(x + width / 2, y, x + width / 2 + 1, y + height, '│');
            Console.SetCursorPosition(x + width / 2, y + height - 3);
          //  Console.Write('┴');
            Console.ForegroundColor = ConsoleColor.White;

            //bottom
            x = 0; y = height + 1;
            drawMenu();

            //dir
            listDirectories(@"C:\");
        }
        public static void listDirectories(string p) { 
            Controller.Drawer(p);
        }
        
        
        public static void drawMenu() {
            Console.SetCursorPosition(x, y);
            bottomMenu("F1", "Left", false);
            bottomMenu(" F2", "Right", false);
            bottomMenu(" F3", "Edit", false);
            bottomMenu(" F4", "View", false);
            bottomMenu(" F5", "Create", false);
            bottomMenu(" F6", "Delete", false);
            bottomMenu("  F7", "Find", false);
            bottomMenu(" F8", "Mkdir", false);
            bottomMenu(" F9", "Tree", false);
            bottomMenu(" F10", "Exit", true);
        }

        public static void draw(int fromX, int fromY, int toEndx, int toEndy, char alpha = ' ') {
            for (int i = fromX; i < toEndx; i++) {
                for (int j = fromY; j < toEndy; j++) {
                    Console.SetCursorPosition(i, j);
                    Console.Write(alpha);
                }
            }
        }


        public static void bottomMenu(string number, string text, bool flag) {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(number);
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.ForegroundColor = ConsoleColor.White;

            if (text.Length < block) {
                string str = text;
                int len = block - text.Length;
                for (int i = 0; i < len; i++) str += " ";
                text = str;
            }
            else text = text.Substring(0, block);
            if (!flag) {
                Console.Write(text);
                cnt += text.Length;
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.DarkBlue;
                Console.BackgroundColor = ConsoleColor.DarkCyan;
                Console.SetCursorPosition(cnt, y);
            }
            else Console.Write(text.Substring(0, text.Length - 4));
        }
    }
}
