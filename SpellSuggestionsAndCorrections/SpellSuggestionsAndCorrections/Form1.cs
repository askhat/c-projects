﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using SpellSuggestionsAndCorrections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpellSuggestionsAndCorrections
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (SpellCorrector.flag == false) 
            {
                listBox2.Items.Clear();
                listBox1.Items.Clear();
            }
            SpellCorrector.flag = true;
            textBox1.KeyDown += new KeyEventHandler(tb_KeyDown);    
        }

        public void tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space && SpellCorrector.flag == true)
            {
                AutoClosingMessageBox.Show("Space typed!", "Notification", 100);
                string str = textBox1.Text;
                //MessageBox.Show(str + " " + str.Length);
                //MessageBox.Show(textBox1.Text + " " + str.Length);
                string textBoxWithoutSpace = str.Substring(0, str.Length - 1);

                //MessageBox.Show(substringed + " "  + substringed.Length);
                string word = SpellCorrector.ReadFromTextBox(textBoxWithoutSpace);// TextBoxSpellCorrector.ReadFromTextBox(textBox1.Text);
                if (!string.IsNullOrEmpty(word))
                {
                    List<SpellCorrector.suggestItem> corrections = SpellCorrector.Correct(word, "");
                    foreach (var correction in corrections)
                    {
                        listBox1.Items.Add(correction.term + " " + correction.distance.ToString());
                    }
                    if (!corrections.Any()) //nothing
                    {
                        listBox1.Items.Add("No corrections :(");
                    }
                    //listBox1.Items.Clear();
                }
                //TRIE
                Trie t = Init.t;
                TrieNode result = t.isPath(word);
                if (result == null) 
                { // there is no path
                    listBox2.Items.Add("No suggestions :(");
                }
                else {
                    List<String> mySuggestions = t.Suggestions(word);
                    foreach (var item in mySuggestions) 
                    {
                        listBox2.Items.Add(item);
                    }
                }
                SpellCorrector.flag = false;
            }
        }
        private void changeText(ListBox listbox) {
            string str = listbox.SelectedItem.ToString();
            string cur = "";
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] != ' ') cur += str[i];
                else break;
            }
            string text = textBox1.Text.Substring(0, textBox1.Text.Length - 1);
            string changingWord = "";
            int index = text.Length - 1;
            for (int i = text.Length - 1; i >= 0; i--)
            {
                if (text[i] != ' ')
                {
                    changingWord += text[i];
                    index = i;
                }
                else break;
            }

            // textBox1.Text.Remove(0, )
            //changing TextBox => 
            string newText = textBox1.Text.Remove(index, changingWord.Length + 1) + " " + cur + " ";
            textBox1.Clear();
            textBox1.Text += newText;
        }
        private bool isSomethingSelected(ListBox listbox) 
        {
            bool flag = false;
            for (int i = 0; i < listbox.Items.Count; i++)
            {
                if (listbox.GetSelected(i))
                {
                    flag = true;
                }
            }
            //flag is true if something was selected, otherwise false
            return flag;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(listBox1.SelectedItem.ToString(s));
            //check which listbox is selected
            if (isSomethingSelected(listBox1)) 
            {
                changeText(listBox1);
            }
            else if(isSomethingSelected(listBox2)) 
            {
                changeText(listBox2);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
