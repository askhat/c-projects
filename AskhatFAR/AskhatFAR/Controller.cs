﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using System.Windows.Forms;

namespace AskhatFAR {
    public static class Controller {
        public static string current_path = "";
        public static int counter = 0;
        public static int footerPos = 0;
        public static ArrayList DirectoryList = new ArrayList();

        public static string Open() {
            string path = "";
            bool clear = true;
            for (int i = 0; i < DirectoryList.Count; i++) {
                Position cc = (Position)DirectoryList[i];
                if (cc.current) {
                    if (cc.isDirectory()) path = cc.destination;
                    else if (cc.isFile()) {
                        clear = false;
                        System.Diagnostics.Process.Start("notepad", cc.destination);
                    }
                }
            }
            if (clear) {
                if (hasReadAccess(path)) {
                    for (int i = 0; i < DirectoryList.Count; i++) {
                        Position cc = (Position)DirectoryList[i];
                        cc.clearLine(ConsoleColor.DarkBlue);
                    }
                    DirectoryList.Clear();
                    Drawer(path);
                }
                else {
                    Drawer(current_path.Substring(0, current_path.Length - 1));
                    return current_path.Substring(0, current_path.Length - 1);
                }
            }
            return path;
        }
        public static void Down() {
            for (int i = 0; i < DirectoryList.Count; i++) {
                Position ll = (Position)DirectoryList[i];
                if (ll.current) {
                    ll.current = false;
                    ll.changeBackground(ConsoleColor.DarkBlue);
                    Position nnll = (Position)DirectoryList[(i + 1) % DirectoryList.Count];
                    nnll.current = true;
                    nnll.changeBackground(ConsoleColor.DarkCyan);
                    Console.BackgroundColor = ConsoleColor.DarkBlue;
                    break;
                }
            }
        }
        public static void Up() {
            for (int i = 0; i < DirectoryList.Count; i++) {
                Position ll = (Position)DirectoryList[i];
                if (ll.current) {
                    ll.current = false;
                    ll.changeBackground(ConsoleColor.DarkBlue);
                    Position nnll = (Position)DirectoryList[(i == 0 ? DirectoryList.Count - 1 : i - 1)];
                    nnll.current = true;
                    nnll.changeBackground(ConsoleColor.DarkCyan);
                    Console.BackgroundColor = ConsoleColor.DarkBlue;
                    break;
                }
            }
        }

        public static void CreateDirectory(string path, string name) {
            DirectoryInfo dir = new DirectoryInfo(path + "\\" + name);
            if (!dir.Exists) {
                dir.Create();
                Design.listDirectories(path + "\\");
                MessageBox.Show("CREATED!!!");
            }
        }

        public static void DeleteItem(string curDir) {
            bool status = false;
            for (int i = 0; i < DirectoryList.Count; i++) {
                //find
                Position ln = (Position)DirectoryList[i];
                if (ln.current) {
                    ln.removeCurrent();
                    status = true;
                }
                if (status) {
                    ln.deepClear();
                    Design.listDirectories(curDir + "\\");
                    break;
                }
            }           
        }

        public static void ButtonPress() {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.SetCursorPosition(0, Design.height);
            string str = "", curDir = @"C:\";
            while (true) {
                ConsoleKeyInfo button = Console.ReadKey();
                Console.BackgroundColor = ConsoleColor.Black;
                switch (button.Key) {
                    case ConsoleKey.Enter:
                        curDir = Open();
                        break;
                    case ConsoleKey.DownArrow:
                        Down();
                        break;
                    case ConsoleKey.UpArrow:
                        Up();
                        break;
                    case ConsoleKey.F5:
                        CreateDirectory(curDir, str);
                        break;
                    case ConsoleKey.F6:
                        DeleteItem(curDir);
                        break;
                    case ConsoleKey.F7:
                        Finder.Searcher(curDir + "\\", str);
                        break;
                    default:
                        int leftCursor = Console.CursorLeft;
                        int rightCursor = Console.CursorTop;
                      /*  MessageBox.Show(leftCursor + " " + rightCursor);
                        Console.SetCursorPosition(0, Design.height - 8);
                        Console.WriteLine("str " + str.Length);
                        Console.WriteLine("left before " + leftCursor);
                        Console.WriteLine("top before " + rightCursor);
                        Console.WriteLine(str);
                        Console.WriteLine("counter " + counter);
                        Console.WriteLine(curDir);*/
                        if (counter >= 7) {
                            for (int i = 0; i < leftCursor; i++) {
                                Console.SetCursorPosition(i, Design.height);
                                Console.Write(' ');
                            }
                            Console.SetCursorPosition(0, Design.height);
                            counter = 0;
                            str = "";
                            break;
                        }
                        if (Char.IsLetter(button.KeyChar)) {
                            str += button.KeyChar;
                            counter++;
                            Console.SetCursorPosition(leftCursor - 1, rightCursor);
                            Console.Write(button.KeyChar);
                        }
                        else if (button.Key.Equals(ConsoleKey.Backspace)) {
                            Console.SetCursorPosition(leftCursor, rightCursor);
                            Console.Write(' ');
                            Console.SetCursorPosition(leftCursor, rightCursor);
                            counter--;
                            //MessageBox.Show("BAAAAACKSPACE@!");
                        }
                        else  {
                            //MessageBox.Show("ASDASDASD");
                            Console.SetCursorPosition(leftCursor - 1, rightCursor);
                            Console.Write(' ');
                            Console.SetCursorPosition(leftCursor - 1, rightCursor);
                        }
                        break;
                    }
                    if (counter < 0) counter = 0;
                    Console.SetCursorPosition(counter, Design.height);
                }
         }


        public static bool hasReadAccess(string _path_) {
            if (Directory.Exists(_path_)) {
                try {
                    System.Security.AccessControl.DirectorySecurity ds = Directory.GetAccessControl(_path_);
                    DirectoryInfo d = new DirectoryInfo(_path_);
                    try {
                        d.GetFiles();
                        d.GetDirectories();
                    }
                    catch (StackOverflowException e) {
                        return false;
                    }
                    return true;
                }
                catch (UnauthorizedAccessException e) {
                    return false;
                }
            }
            return false;
        }
        public static string getParent(DirectoryInfo dir) {
            if (dir.Parent == null) {
                return dir.FullName;
            }
            return dir.Parent.FullName;
        }

        public static void Drawer(string path) { 
            if (Directory.Exists(path)) {
                DirectoryInfo current_dir = new DirectoryInfo(path);
                if (hasReadAccess(path)) {
                    FileInfo[] files = current_dir.GetFiles();
                    DirectoryInfo[] dirs = current_dir.GetDirectories();
                    if (DirectoryList.Count != 0) {
                        DirectoryList.Clear();
                    }
                    int _x_ = 0;
                    int _y_ = 1;
                    int _max_height = Design.height - 6;
                    bool _overloading = false;
                    int maximal = 0;
                    DirectoryList.Add(new Position(_x_ + 1, _y_ + 2 - 1, (Design.width - 2) / 2 - 1, true, "..", getParent(current_dir)));
                    foreach (DirectoryInfo iteration in dirs) {
                        if (_y_ > _max_height) {
                            _x_ = (Design.width - 2) / 2 + 1;
                            _y_ = 0;
                            _overloading = true;
                        }
                        Position item = new Position(1 + _x_, 2 + _y_++, (Design.width - 2) / 2 - 1, false, iteration.Name, iteration.FullName);
                        DirectoryList.Add(item);
                    }
                    int files_number = 0;
                    foreach (FileInfo iteration in files) {
                        if (_y_ > _max_height) {
                            _x_ = (Design.width - 2) / 2 + 1;
                            _y_ = 0;
                            _overloading = true;
                        }
                        files_number++;
                        Position item = new Position(1 + _x_, 2 + _y_++, (Design.width - 2) / 2 - 1, false, iteration.Name, iteration.FullName);
                        DirectoryList.Add(item);
                    }
                    Operate();
                }
            }
        }
        private static void Operate() {
            for (int i = 0; i < DirectoryList.Count; i++) {
                Position current = (Position)DirectoryList[i];
                if (i < Design.height * 2 - 10) current.drawLine();
            }
        }


    }
}
