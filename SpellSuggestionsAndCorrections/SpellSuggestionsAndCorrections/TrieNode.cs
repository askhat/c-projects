﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpellSuggestionsAndCorrections {
    public class TrieNode {
        public SortedDictionary<char, TrieNode> children;
        public bool endOfWord;
        public TrieNode() {
            children = new SortedDictionary<char, TrieNode>();
            endOfWord = false;
        }
    }
    public class Trie {
        private TrieNode root;
        //public ;  // distance, word
        //;
        public Trie() {
            root = new TrieNode();
            //result = new SortedDictionary<int, String>();
            //used = new HashSet<Pair<String, bool>>();
        }
        public void Insert(String word) {
            TrieNode current = root;
            for (int i = 0; i < word.Length; i++) 
            {
                char curChar = word[i];
                if (!current.children.ContainsKey(curChar)) 
                {
                    TrieNode node = new TrieNode();
                    current.children.Add(curChar, node);
                    current = node;
                }
                else 
                {
                    current = current.children[curChar];
                }
            }
            current.endOfWord = true;
        }

        public bool Search(String word) 
        {
            TrieNode current = root;
            for (int i = 0; i < word.Length; i++) 
            {
                char curChar = word[i];
                if (!current.children.ContainsKey(curChar)) 
                {
                    return false;
                }
                current = current.children[curChar];
            }
            return current.endOfWord;
        }

        public TrieNode isPath(String word) 
        {
            TrieNode current = root;
            for (int i = 0; i < word.Length; i++) 
            {
                char curChar = word[i];
                if (!current.children.ContainsKey(curChar)) 
                {
                    return null;
                }
                current = current.children[curChar];
            }
            return current;
        }
        //Pair<String, bool> curPair = new Pair<string, bool>(curString, true); used.Add(curPair);
        /*public void DFS(String word, TrieNode current, ref SortedDictionary<int, string> result, Dictionary<String, bool> used, int level) {          
            String curString = word;
            level++;
            if (current.endOfWord == true) {
                result.Add(level, word);
                return;
            }
            if (used.ContainsKey(word))
            {
                if (used[word] == true)
                {
                    level--;
                    return;
                }
            }
            foreach(var item in current.children) { //adjacent
                char addingChar = item.Key;
                curString += addingChar;
                if (!used.ContainsKey(curString)) {
                    used.Add(curString, true);
                }
                if (!current.children.ContainsKey(addingChar)) return;
                else {
                    current = current.children[addingChar]; // to
                    DFS(curString, current, ref result, used, level);   
                }
                                
            }
        }
        */
        public List<String> Suggestions(String word) 
        {
            List<String> result = new List<String>();
            Dictionary<String, bool> used = new Dictionary<String, bool>();
            TrieNode current = isPath(word);
            if (current.endOfWord) 
            {
                result.Add(word);
            }
            else  
            {
                DFS(word, current, ref result, ref used);
            }
            return result;
        }
        public void DFS(String word, TrieNode cur, ref List<String> result, ref Dictionary<String, bool> used) 
        {
            used.Add(word, true);
            if (cur.endOfWord == true) 
            {
                result.Add(word);
            }
            foreach (var item in cur.children) 
            {
                char addingChar = item.Key;
                string nextWord = word + addingChar;
                if (cur.children.ContainsKey(addingChar)) 
                {
                    cur = cur.children[addingChar];
                }
                else 
                {
                    continue;
                }
                if (!used.ContainsKey(nextWord)) 
                {
                    DFS(nextWord, cur, ref result, ref used);
                }
            }
        }
    }
}
