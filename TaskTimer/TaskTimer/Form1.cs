﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using rtChart;
using System.Diagnostics;

namespace TaskTimer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        static kayChart cpuData;
        static kayChart anotherData;
        public int adder1 = 1, adder2 = 0, adder3 = 0, s1 = 0, s2 = 0, s3 = 0, cnt = 0;
        List<string> list = new List<string>();
        bool flag1 = false, flag2 = false, flag3 = false;
        
        public string Adjustment(int seconds)
        {
            string hours = "", minutes = "", sec = "";
            int h = seconds / 3600;
            int left = seconds - h * 3600;
            int m = left / 60;
            left -= m * 60;
            int s = left;

            if (h < 10) {
                hours += "0";
                hours += h.ToString();
            }
            else
            {
                hours += h.ToString();
            }
            if (m < 10)
            {
                minutes += "0";
                minutes += m.ToString();
            }
            else
            {
                minutes += m.ToString();
            }
            if (s < 10)
            {
                sec += "0";
                sec += s.ToString();
            }
            else {
                sec = s.ToString();
            }
            string time = hours.ToString() + ":" + minutes.ToString() + ":" + sec.ToString();
            return time;
        }

        public void remove_row(TableLayoutPanel panel, int row_index_to_remove)
        {
            if (row_index_to_remove >= panel.RowCount)
            {
                return;
            }

            // delete all controls of row that we want to delete
            for (int i = 0; i < panel.ColumnCount; i++)
            {
                var control = panel.GetControlFromPosition(i, row_index_to_remove);
                panel.Controls.Remove(control);
            }

            // move up row controls that comes after row we want to remove
            for (int i = row_index_to_remove + 1; i < panel.RowCount; i++)
            {
                for (int j = 0; j < panel.ColumnCount; j++)
                {
                    var control = panel.GetControlFromPosition(j, i);
                    if (control != null)
                    {
                        panel.SetRow(control, i - 1);
                    }
                }
            }

            // remove last row
            //panel.RowStyles.RemoveAt(panel.RowCount - 1);
           // panel.RowCount--;
        }
        private void ProcessKiller(int row)
        {
            var controller = tableLayoutPanel1.GetControlFromPosition(0, row);
            foreach (var process in Process.GetProcessesByName(controller.Text))
            {
                process.Kill();
            }
        }
        void MyButtonClick(object sender, EventArgs e)
        {
            //Button cur = (Button)sender;
           // MessageBox.Show(cur.
            int row = int.Parse(((Button)sender).Name.ToString());
            ProcessKiller(row);
            remove_row(tableLayoutPanel1, row);
            
          //  MessageBox.Show((      (Button)sender).Name.ToString() + " was pressed!");
            //MessageBox.Show("I AM CLICKED!" + currentButtonIndex);
            /*Button currentButton = tableLayoutPanel1.GetControlFromPosition();
            int currentButtonIndex = int.Parse(cur);
            remove_row(tableLayoutPanel1, currentButtonIndex);
            cnt = currentButtonIndex;*/
            // First Button Clicked
        }
        
        private void timer1_Tick(object sender, EventArgs e)
        {
         //   MessageBox.Show("ASDASD");
            if (progressBar1.Value < progressBar1.Maximum)
            {
                progressBar1.Increment(adder1);
                s1++;
                label4.Text = Adjustment(s1);
            }
            else if(flag1 != true)
            {
                var current = tableLayoutPanel1.GetControlFromPosition(0, 0);
                flag1 = true;
                WMPLib.WindowsMediaPlayer wplayer = new WMPLib.WindowsMediaPlayer();
                MessageBox.Show("You have finished " + current.Text);
                wplayer.URL = @"C:\Music\griby.mp3";
                MessageBox.Show("WELL DONE!:)");
                wplayer.controls.play();
                ProcessKiller(0);
                //PauseClass p = new PauseClass();
                // wplayer.controls.stop();



            }
           
          //  progressBar1.Value++;

        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (progressBar2.Value < progressBar2.Maximum)
            {
                progressBar2.Increment(adder2);
                s2++;
                label5.Text = Adjustment(s2);
            }
            else if(flag2 != true)
            {
                var current = tableLayoutPanel1.GetControlFromPosition(0, 1);
                flag2 = true;
                WMPLib.WindowsMediaPlayer wplayer = new WMPLib.WindowsMediaPlayer();
                MessageBox.Show("You have finished " + current.Text);
                wplayer.URL = @"C:\Music\griby.mp3";
                MessageBox.Show("WELL DONE!:)");
                ProcessKiller(1);
                wplayer.controls.play();
            }
        }
        private void timer3_Tick(object sender, EventArgs e)
        {
            if (progressBar3.Value < progressBar3.Maximum)
            {
                progressBar2.Increment(adder3);
                s3++;
                label6.Text = Adjustment(s3);
            }
            else if(flag3 != true)
            {
                var current = tableLayoutPanel1.GetControlFromPosition(0, 2);
                flag3 = true;
                WMPLib.WindowsMediaPlayer wplayer = new WMPLib.WindowsMediaPlayer();
                MessageBox.Show("You have finished " + current.Text);
                wplayer.URL = @"C:\Music\griby.mp3";
                MessageBox.Show("WELL DONE!:)");
                ProcessKiller(2);
                wplayer.controls.play();
            }
        }
        int inner = 0;
        private void buttonClicked(object sender, EventArgs e) {
            
            for (int i = 0; i < tableLayoutPanel1.RowCount; i++)
            {
                var control = tableLayoutPanel1.GetControlFromPosition(0, i);
                if (control == null)
                {
                    cnt = i;
                    break;
                }
            }
            string text = textBox1.Text;
            Process process = new Process();
            process.StartInfo.FileName = text;
            bool isThere = false;
            for (int i = 0; i < list.Count; i++)
            {
                if(list[i] == text)
                {
                    isThere = true;
                    break;
                }

            }
            if(isThere) {
                process.Start();
            }
           
            if (inner == 0) {
                progressBar1.Visible = true;
                label4.Visible = true;
                decimal total = (numericUpDown1.Value * 60 + numericUpDown2.Value) * 60 + numericUpDown3.Value;
                adder1 = progressBar1.Maximum / (int)(total);
                timer1.Start();
                RowStyle temp = tableLayoutPanel1.RowStyles[cnt];
                // ProgressBar p = new ProgressBar();

                Button button = new Button();
              //  Label label = new Label();
              //  label.Text = "00:00:00";           
                button.Name = cnt.ToString();
                button.Font = new Font("Arial", 10);
                button.Text = "Delete Task " + cnt;
                //currentButtonIndex = cnt;
                button.Width = 90;
                // MessageBox.Show(cnt.ToString());
                
                tableLayoutPanel1.Controls.Add(new Label() { Text = textBox1.Text, Font = new Font("Arial", 13) }, 0, cnt);
                //  tableLayoutPanel1.Controls.Add(p, 1, cnt);
                tableLayoutPanel1.Controls.Add(label4, 2, cnt);
                tableLayoutPanel1.Controls.Add(button, 3, cnt);
                button.Click += new EventHandler(MyButtonClick);
                
               
               // decimal total = (numericUpDown1.Value * 60 + numericUpDown2.Value) * 60;
                // MessageBox.Show(total.ToString());
                //int adder = p.Maximum / (int)total;
               
            }
            else if (inner == 1)
            {
                //MessageBox.Show(inner.ToString());
                progressBar2.Visible = true;
                label5.Visible = true;
                timer2.Start();
                decimal total = (numericUpDown1.Value * 60 + numericUpDown2.Value) * 60 + numericUpDown3.Value;
                adder2 = progressBar2.Maximum / (int)(total);

                RowStyle temp = tableLayoutPanel1.RowStyles[cnt];
                // ProgressBar p = new ProgressBar();

                Button button = new Button();
                // Label label = new Label();
                //label.Text = "00:00:00";

                //p.Width = 200;
                //p.Minimum = 0;
                //p.Maximum = 120;
                button.Name = cnt.ToString();
                button.Font = new Font("Arial", 10);
                button.Text = "Delete Task " + cnt;
                //currentButtonIndex = cnt;
                button.Width = 90;
                // MessageBox.Show(cnt.ToString());
                // textBox1.Font = new Font("Arial", 15);
                tableLayoutPanel1.Controls.Add(new Label() { Text = textBox1.Text, Font = new Font("Arial", 13) }, 0, cnt);
                //  tableLayoutPanel1.Controls.Add(p, 1, cnt);
                tableLayoutPanel1.Controls.Add(label5, 2, cnt);
                tableLayoutPanel1.Controls.Add(button, 3, cnt);
                button.Click += new EventHandler(MyButtonClick);
            }
            else if (inner == 2)
            {
                //MessageBox.Show(inner.ToString());
                progressBar3.Visible = true;
                label6.Visible = true;
                timer3.Start();
                decimal total = (numericUpDown1.Value * 60 + numericUpDown2.Value) * 60 + numericUpDown3.Value;
                adder3 = progressBar3.Maximum / (int)(total);

                RowStyle temp = tableLayoutPanel1.RowStyles[cnt];
                // ProgressBar p = new ProgressBar();

                Button button = new Button();
                // Label label = new Label();
                //label.Text = "00:00:00";

                //p.Width = 200;
                //p.Minimum = 0;
                //p.Maximum = 120;
                button.Name = cnt.ToString();                     
                button.Font = new Font("Arial", 10);         
                button.Text = "Delete Task " + cnt;
                //currentButtonIndex = cnt;
                button.Width = 90;
                // MessageBox.Show(cnt.ToString());
                tableLayoutPanel1.Controls.Add(new Label() { Text = textBox1.Text, Font = new Font("Arial", 13) }, 0, cnt);
                //  tableLayoutPanel1.Controls.Add(p, 1, cnt);
                tableLayoutPanel1.Controls.Add(label6, 2, cnt);
                tableLayoutPanel1.Controls.Add(button, 3, cnt);
                button.Click += new EventHandler(MyButtonClick);
            }
            inner++;


            
            //Task.WaitAll(task1, task2);
            /*  for (int i = 0; i < N; i++)
            {
                if (flags[i] == false)
                {
                    if (i == 0)
                    {
                        MessageBox.Show(i.ToString());
                        ParallelClick1(sender, e);
                    }
                    else if (i == 1)
                    {
                        MessageBox.Show(i.ToString());
                         Parallel.Invoke(() =>
                             {
                                 //Console.WriteLine("Begin first task...");
                                 ParallelClick1(sender, e);
                             },  // close first Action

                             () =>
                             {
                                // Console.WriteLine("Begin second task...");
                                 ParallelClick2(sender, e);
                             } //close second Action
                        );
                       // ParallelClick2(sender, e);
                    }
                    else
                    {
                        MessageBox.Show(i.ToString());
                        ParallelClick3(sender, e);
                    }
                    break;
                } 
            }*/
           /* if (flag == false && inner != 1)
            {
                MessageBox.Show("I AM IN ANOTHER " + cnt);
                ParallelClick(sender, e);

            }*/
            
           // MessageBox.Show(total.ToString());
            /*int adder = p.Maximum / (int)total;
            PauseClass pause = new PauseClass();
            cnt++;
            int seconds = 0;
            while (p.Value < p.Maximum) {
                p.Increment(adder);
                label.Text = Adjustment(seconds);
                pause.Pause(1000);
                seconds++;
            }*/
            //flag = true;
           // MessageBox.Show("WELL DONE");
            
        }
















        Timer t = new Timer();

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Interval = 1000;
            timer2.Interval = 1000;
            timer3.Interval = 1000;
            timer4.Interval = 400;
           // timer4.Start();
            label4.Text = "00:00:00";
            label5.Text = "00:00:00";
            label6.Text = "00:00:00";

            label4.Font = new Font("Arial", 11);
            label5.Font = new Font("Arial", 11);
            label6.Font = new Font("Arial", 11);


            label4.Visible = false;
            label5.Visible = false;
            label6.Visible = false;
            progressBar1.Visible = false;
            progressBar2.Visible = false;
            progressBar3.Visible = false;
            progressBar1.Maximum = 600;
            progressBar1.Minimum = 0;

            progressBar1.Width = 190;
            progressBar2.Width = 190;
            progressBar3.Width = 190;
            //cpuData = new kayChart(chart1, 60);
            //anotherData = new kayChart(chart1, 60);

           // cpuData.serieName = "CPU";
            cpuData = new kayChart(chart1, 100);
            cpuData.serieName = "CPU";
            Task.Factory.StartNew(() =>
            {
                //Task.WaitAll(task1, task2);
                // anotherData.updateChart(generateRandomNumber, 100);
                cpuData.updateChart(updateWithCPU, 100);
            });
            //anotherData.serieName = "AnotherData";
            // t.Interval = 1000;  //in milliseconds

            //t.Tick += new EventHandler(this.t_Tick);
            list.Add("paint");
            list.Add("calc");
            list.Add("chrome");
            list.Add("explorer");
            list.Add("regedit");


        }
         private void t_Tick(object sender, EventArgs e) {
            //get current time
                int hh = DateTime.Now.Hour;
                int mm = DateTime.Now.Minute;
                int ss = DateTime.Now.Second;

                //time
                string time = "";

                //padding leading zero
                if (hh < 10)
                {
                    time += "0" + hh;
                }
                else
                {
                    time += hh;
                }
                time += ":";

                if (mm < 10)
                {
                    time += "0" + mm;
                }
                else
                {
                    time += mm;
                }
                time += ":";

                if (ss < 10)
                {
                    time += "0" + ss;
                }
                else
                {
                    time += ss;
                }

                //update label
                //label1.Text = time;
            }
         
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

       
        private void button2_Click_1(object sender, EventArgs e)
        {
            cpuData = new kayChart(chart1, 120);
            cpuData.serieName = "CPU";
            Task.Factory.StartNew(() =>
            {
                //Task.WaitAll(task1, task2);
                // anotherData.updateChart(generateRandomNumber, 100);
                cpuData.updateChart(updateWithCPU, 100);
            });
            //timer4.Start();
           /* kayChart cpuData = new kayChart(chart1, 60);
           
            kayChart anotherData = new kayChart(chart1, 60);
            

            var task1 = Task.Factory.StartNew(() => cpuData.updateChart(updateWithCPU, 100));
            var task2 = Task.Factory.StartNew(() => anotherData.updateChart(generateRandomNumber, 100));
                                                            ы
            Task.Factory.StartNew(() =>
            {
                Task.WaitAll(task1, task2);
                // anotherData.updateChart(generateRandomNumber, 100);
                //cpuData.updateChart(updateWithCPU, 1000);
            });
        */   
    }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            cpuData.updateChart(updateWithCPU, 200);
            //anotherData.updateChart(generateRandomNumber, 200);
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {
                                                                                                      
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {

        }

        
        PerformanceCounter cpu = new PerformanceCounter("Processor Information", "% Processor Time", "_Total");
        private double updateWithCPU()
        {
            return cpu.NextValue();
        }
        private double generateRandomNumber()
        {
            Random rnd = new Random();
            return rnd.Next(0, 5000);
        }

    }

}
