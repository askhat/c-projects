﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskhatMidtermHospital
{
    [Serializable()]    
    class Doctor : Person
    {
        public string Education { get; set; }
        public int Certificates { get; set; }
        public int Languages { get; set; }

        public Doctor(string _firstName, string _lastName, int _age, string _phoneNumber, Genders gender, string _education, int _certificates, int _languages)
            : base(_firstName, _lastName, _age, _phoneNumber, gender)
        {
            Education = _education;
            Certificates = _certificates;
            Languages = _languages;

        }
    }
}
