﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace AskhatFAR {
    public static class Finder {
        public static void Searcher(string sDir, string pattern, bool flag) {
            foreach (string dir in Directory.GetDirectories(sDir)) {   
                try {

                     foreach (string file in Directory.GetFiles(dir)) {
                        string txtFile = Path.GetFileName(file);
                        FileInfo ff = new FileInfo(dir + "\\" + txtFile);
                        if (flag) {
                            if (txtFile.Contains(pattern)) {
                                MessageBox.Show(dir + "\\" + txtFile);
                                return;
                            }
                        }
                        else
                        {
                            //using REGEX
                            /*foreach (var line in File.ReadLines(ff.FullName).Where(line => regex.Match(line).Success))  {
                                File.AppendAllText("file to write to", line + Environment.NewLine);
                            }*/
                            //using contains
                            string[] lines = File.ReadAllLines(ff.FullName);
                            foreach (string curLine in lines)
                            {
                                if (curLine.Contains(pattern))
                                {
                                    MessageBox.Show("FOUND! " + ff.FullName);
                                    //checker = true;
                                    return;
                                }
                            }
                            //Console.WriteLine(txtFile);
                        }
                        Searcher(dir, pattern, flag);
                    }
                   
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}