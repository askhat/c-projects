﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AskhatFAR {
    class Position {
        public int x, y, length;
        public bool current;
        public string name, destination;
        public Position(int x, int y, int length, bool isCurrent, string name, string destination) {
            this.x = x;
            this.y = y;
            this.length = length;
            this.current = isCurrent;
            this.name = name;
            this.destination = destination;
        }
        public void changeBackground(ConsoleColor color) {
            Console.SetCursorPosition(this.x, this.y);
            Console.BackgroundColor = color;
            if (this.isFile()) Console.ForegroundColor = this.getExtensionColor((new FileInfo(this.destination)).Extension);
            if (this.isDirectory()) {
                DirectoryInfo dr = new DirectoryInfo(this.destination);
                if ((dr.Attributes & FileAttributes.Hidden) != 0) {
                    if (current) Console.ForegroundColor = ConsoleColor.Yellow;
                    else Console.ForegroundColor = ConsoleColor.DarkGreen;
                }
            }
            Console.WriteLine(this.currentString(this.name, this.length));
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.DarkBlue;
        }
        public void changeBackground(ConsoleColor color, ConsoleColor font) {
            Console.SetCursorPosition(this.x, this.y);
            Console.BackgroundColor = color;
            Console.ForegroundColor = font;
            Console.WriteLine(this.currentString(this.name, this.length));
            //def
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.DarkBlue;
        }
        public void clearLine(ConsoleColor color) {
            Console.SetCursorPosition(this.x, this.y);
            Console.BackgroundColor = color;
            Console.WriteLine(this.currentString("", this.length));
            Console.BackgroundColor = ConsoleColor.DarkBlue;
        }

        public void drawLine() {
            string drawing = currentString(this.name, this.length);
            Console.SetCursorPosition(this.x, this.y);
            if (this.current) Console.BackgroundColor = ConsoleColor.DarkGreen; ;
            if (this.isFile()) Console.ForegroundColor = this.getExtensionColor((new FileInfo(this.destination)).Extension);
            if (this.isDirectory()) {
                DirectoryInfo dr = new DirectoryInfo(this.destination);
                if ((dr.Attributes & FileAttributes.Hidden) != 0) {
                    if (this.current) Console.ForegroundColor = ConsoleColor.Yellow;
                    else Console.ForegroundColor = ConsoleColor.DarkGreen;
                }
            }
            Console.WriteLine(drawing);
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.DarkBlue;
        }

        public void removeCurrent() {
            try {
                var dest = this.destination;
                if (this.isFile()) {
                    FileInfo file = new FileInfo(dest);
                    if (!file.IsReadOnly) File.Delete(dest);
                    this.clearLine(ConsoleColor.DarkBlue);
                    this.deepClear();
                }
                else if (this.isDirectory()) {
                    DirectoryInfo dir = new DirectoryInfo(dest);
                }
            }
            catch (Exception e) {
                Console.Write(e.Message);
            }
        }
        private string currentString(string line, int length) {
            if (line.Length > length) return line.Substring(0, length);
            else if (line.Length == length) return line;
            else {
                string tp = "";
                for (int i = 0; i < length - line.Length; i++)
                    tp += " ";
                return line + tp;
            }
        }
        private ConsoleColor getExtensionColor(string curExtension) {
            ConsoleColor output;
            switch (curExtension) {
                case ".sys":
                    output = ConsoleColor.DarkGreen;
                    break;
                case ".ini":
                    output = ConsoleColor.DarkGreen;
                    break;
                default:
                    output = ConsoleColor.White;
                    break;
            }
            return output;
        }
        public void deepClear() {
            Console.SetCursorPosition(this.x, this.y);
            for (int i = 0; i < this.length; i++) Console.Write(' ');
        }
        public bool isDirectory() {
            if (Directory.Exists(this.destination)) return true;
            return false;
        }
        public bool isFile() {
            if (File.Exists(this.destination)) return true;
            return false;
        }
    }
}