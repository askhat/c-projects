﻿#include <bits\stdc++.h>
using namespace std;
typedef vector<int> vi;
typedef vector<long long> vll;
typedef vector<vi> vvi;
typedef vector<pair<int, int> > vpi;
typedef pair<int,int> pii;
#define ll long long
#define ull unsigned long long
#define pb push_back
#define sz size()
#define sqr(x) ((x) * (x))
#define mp make_pair
#define F first
#define S second
#define all(c) c.begin(), c.end()
#define rall(c) c.rbegin(), c.rend()
#define tr(container, it) \
 for(typeof(container.begin()) it = container.begin(); it != container.end(); it++)
#define present(container, element) (container.find(element) != container.end()) // set, map
#define cpresent(container, element) (find(all(container), element) != container.end())\\

int checker(int x) {
    if(x <= 30 && x >= 0)      return 1;
    else if(x > 30 && x <= 50) return 2;
    else if(x > 50 && x <= 70) return 3;
    else if(x > 70 && x <= 90) return 4;
    else return 5;
}

string evaluatorFirst(int x) {
    return to_string(abs(1 - (1.0 / 20.0) * (double)x) * 100) + "%";
}
string evaluatorSecond(int x) {
    return to_string(abs((1.0 / 20.0) * (double)x) * 100) + "%";
}

pair<double, double> CloudySunny(int h) {
    pair<double, double> res;
    if(h <= 20) {
        res.F = 0;
        res.S = 1;
    }
    else if(h > 20 && h <= 40) {
        double cloud = (1.0 / 30.0) * (double)(h - 20);
        res.F = cloud;
        double sunny = 1.0 - (1.0 / 20.0) * (double)(h - 20);
        res.S = sunny;
    }
    else if(h > 50 && h < 80) {
        double cloud = 1.0 - (1.0 / 30.0) * (double)(h - 50);
        res.F = cloud;
        res.S = 0;
    }
    return res;
}


pair<double, double> WarmCool(int x) {
    pair<double, double> res;
    if(x > 30 && x <= 50) {
        res.S = (1.0 / 20.0) * (double)(x - 30);
        res.F = 0;
    }
    else if(x > 50 && x <= 70) {
        res.S = 1.0 - (1.0 / 20.0) * (double)(x - 50);
        res.F = (1.0 / 20.0) * (double)(x - 50);
    }
    else if(x > 70 && x < 90) {
        res.F = 1.0 - (1.0 / 20.0) * (double)(x - 70);
        res.S = 0;
    }
    return res;
}


int main(int argc, char const *argv[]) {
    freopen("in", "r", stdin);
    freopen("out", "w", stdout);
    //cout << "Please, write your value: " << endl;
    int x, h;
    double fast, slow, average;
    while(cin >> x >> h) {
        pair<double, double> warmCool = WarmCool(x);
        pair<double, double> cloudSun = CloudySunny(h);
        //conjuction of values in Fuzzy Logic
        fast = min(cloudSun.S, warmCool.F);
        slow = min(cloudSun.F, warmCool.S);
        average = (slow * 250 + 750 * fast) / 9.0;
        //cout << cloudSun.S << endl;
        //cout << "fast " << fast << endl;
        //cout << "slow " << slow << endl;
        switch(checker(x)) {
        case 1:
            cout << "It is 100% freezing" << endl;
            break;
        case 2:
            cout << "It is " << evaluatorFirst(x - 30) << " freezing and " << evaluatorSecond(x - 30) << " cool " << endl;
            break;
        case 3:
            cout << "It is " << evaluatorFirst(x - 50) << " warm and " << evaluatorSecond(x - 50) << " cool " << endl;
            break;
        case 4:
            cout << "It is " << evaluatorFirst(x - 70) << " warm and " << evaluatorSecond(x - 70) << " hot " << endl;
            break;
        case 5:
            cout << "It is really hot!) " << endl;
            break;
        }
        cout << "Please, drive at an average of " << average << endl << endl;

    }

    return 0;
}
