﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EndtermAskhat
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int R1 = 0, G1 = 255, B1 = 0, R2 = 0, G2 = 0, B2 = 255, R3 = 255, G3 = 0, B3 = 0;
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void Changer(int r1, int g1, int b1, int r2, int g2, int b2, int addR, int addG, int addB, ref int curR, ref int curG, ref int curB)
        {
            if (r2 > r1 && curR + addR <= 255) curR += addR;
            else if(curR - addR >= 0) curR -= addR;

            if (g2 > g1 && curG + addG <= 255) curG += addG;
            else if(curG - addG >= 0) curG -= addG;

            if (b2 > b1 && curB + addB <= 255) curB += addB;
            else if(curB - addB >= 0) curB -= addB;

           // curR = Math.Abs(curR);
            //curG = Math.Abs(curG);
            //5curB = Math.Abs(curB);

        }
        private void button1_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            Brush b1 = new SolidBrush(Color.FromArgb(R1, G1, B1));
            int curX = 100, curY = 100, curR = R1, curG = G1, curB = B1;
            // g.FillEllipse(b1, curX, curY, 7, 7);
            /*int addR = Math.Abs(R1 - R2) / 40, addB = Math.Abs(B1 - B2) / 40, addG = Math.Abs(G1 - G2) / 40;
            Brush mid = new SolidBrush(Color.FromArgb((R1 + R2) / 2, (B1 + B2) / 2, (G1 + G2) / 2));
            //.FillEllipse(mid, 200, 200, 50, 50);

            for (int i = 0; i < 39; i++) {
                curY += 7;
                Changer(R1, G1, B1, R2, G2, B2, addR, addG, addB, ref curR, ref curG, ref curB);
                Brush currentBrush = new SolidBrush(Color.FromArgb(curR, curG, curB));
                g.FillEllipse(currentBrush, curX, curY, 7, 7);
            }
            curX = 100; curY = 100;
            for (int i = 0; i < 27; i++) {
                curX += 5;
                curY += 5;
                Brush currentBrush = new SolidBrush(Color.FromArgb(curR, curG, curB));
                g.FillEllipse(currentBrush, curX, curY, 7, 7);
            }
           

            curX = 100; curY = 100 + 7 * 40;
            for (int i = 0; i < 28; i++) {
                curX += 5;
                curY -= 5;
                Brush currentBrush = new SolidBrush(Color.FromArgb(R1, G1, B1));
                g.FillEllipse(currentBrush, curX, curY, 7, 7);
            }
          
            curX = 100 + 28 * 5; curY = 240;
            Brush cc = new SolidBrush(Color.FromArgb(R3, G3, B3));
            g.FillEllipse(cc, curX, curY, 7, 7);


            //13 filling
            addR = Math.Abs(R1 - R3) / 27;
            addB = Math.Abs(B1 - B3) / 27;
            addG = Math.Abs(G1 - G3) / 27;
            curR = R1; curG = G1; curB = B1;
            curX = 100; curY = 100;
            for (int i = 0; i < 27; i++) {
                curX += 5;
                curY += 5;

                Changer(R1, G1, B1, R3, G3, B3, addR, addG, addB, ref curR, ref curG, ref curB);
                Brush currentBrush = new SolidBrush(Color.FromArgb(curR, curG, curB));
                g.FillEllipse(currentBrush, curX, curY, 7, 7);
            }

            //23 filling
            curX = 100; curY = 100 + 7 * 40;
            addR = Math.Abs(R2 - R3) / 27;
            addB = Math.Abs(B2 - B3) / 27;
            addG = Math.Abs(G2 - G3) / 27;
            curR = R2; curG = G2; curB = B2;
            for (int i = 0; i < 27; i++) {
                curX += 5;
                curY -= 5;
                Changer(R1, G1, B1, R3, G3, B3, addR, addG, addB, ref curR, ref curG, ref curB);
                Brush currentBrush = new SolidBrush(Color.FromArgb(curR, curG, curB));
                g.FillEllipse(currentBrush, curX, curY, 7, 7);
            }
           */
            ///topdown filling
            curX = 10; curY = 10;
            int cnt = 61, dif = 0;
            //HashSet<int> x = new HashSet<int>();
            //HashSet<int> y = new HashSet<int>()

            for (int i = 0; i < 39; i++)
            {
                curX += 7;
                for (int j = 0; j < cnt; j++)
                {
                    curY += 7;
                    Brush b = new SolidBrush(Color.FromArgb(R1, G1, B1));
                    g.FillEllipse(b, curX, curY, 7, 7);
                   
                   /* if (cnt == 1)
                    {
                        //  MessageBox.Show(curX.ToString());
                        //MessageBox.Show(curY.ToString());
                    }*/
                }
                curY = 15 + dif;
                dif += 7;
                cnt -= 2;
            }

            Brush b3 = new SolidBrush(Color.Red);
            g.FillEllipse(b3, 227, 225, 7, 7);


            //12
            int addR = Math.Abs(R1 - R2) / 61, addB = Math.Abs(B1 - B2) / 61, addG = Math.Abs(G1 - G2) / 61;
            curX = 17; curY = 10;

            for (int i = 0; i < 61; i++)
            {
                curY += 7;
                Changer(R1, G1, B1, R2, G2, B2, addR, addG, addB, ref curR, ref curG, ref curB);
                Brush currentBrush = new SolidBrush(Color.FromArgb(curR, curG, curB));
                g.FillEllipse(currentBrush, curX, curY, 7, 7);
            }

            /*g.FillEllipse(b3, 24, 22, 7, 7);
            g.FillEllipse(b3, 31, 29, 7, 7);
            g.FillEllipse(b3, 38, 36, 7, 7);
            */
          /*  curX = 17;
            curY = 15;
            curR = R1; curB = B1; curG = G1;
            for (int i = 1; i < 31; i++) {
                curX += 7;
                curY += 7;
                int addR13 = Math.Abs(R1 - R3) / 31, addG13 = Math.Abs(G1 - G3) / 31, addB13 = Math.Abs(B1 - B3) / 31;
                 Changer(R1, G1, B1, R2, G2, B2, addR13, addG13, addB13, ref curR, ref curG, ref curB);
               // curR += addR;
                //curG -= addR;
                Brush currentBrush = new SolidBrush(Color.FromArgb(curR, curG, curB));
                g.FillEllipse(currentBrush, curX, curY, 7, 7);

            }




            */
                cnt = 59;
           //topdown   
          


            int curTopR = R1, curTopG = G1, curTopB = B1;
            int curBotR = R2, curBotG = G2, curBotB = B2;

            int currentR = curTopR, currentB = curTopB, currentG = curTopG;

            curX = 17;
            curY = 15;
            
            int def = curY;


            for (int i = 1; i < 39; i++) {
                int addR13 = Math.Abs(R1 - R3) / 39, addG13 = Math.Abs(G1 - G3) / 39, addB13 = Math.Abs(B1 - B3) / 39;
                Changer(R1, G1, B1, R2, G2, B2, addR13, addG13, addB13, ref curTopR, ref curTopG, ref curTopB);

                int addR23 = Math.Abs(R2 - R3) / 39, addG23 = Math.Abs(G2 - G3) / 39, addB23 = Math.Abs(B2 - B3) / 39;
                Changer(R2, G2, B2, R3, G3, B3, addR23, addG23, addG23, ref curBotR, ref curBotG, ref curBotB);

                curX += 7;

                currentR = curTopR;
                currentB = curTopB;
                currentG = curTopG;
                for (int j = 0; j < cnt; j++) {
                    int toR = Math.Abs(curTopR - curBotR) / cnt, toB = Math.Abs(curTopB - curBotB) / cnt, toG = Math.Abs(curTopG - curBotG) / cnt;
                    Changer(curTopR, curTopG, curTopB, curBotR, curBotG, curBotB, toR, toG, toB, ref currentR, ref currentG, ref currentB);
                    Brush currentBrush = new SolidBrush(Color.FromArgb(currentR, currentG, currentB));

                    curY += 7;
                    g.FillEllipse(currentBrush, curX, curY, 7, 7);
                }
                def += 7;
                curY = def;
                cnt -= 2;
                //
                ;
            }




        
        }
    }
}
