﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.Threading.Tasks;

namespace SpellSuggestionsAndCorrections
{
    
    public static class Init
    {
        public static Trie t = new Trie();
        public static void Initializer() 
        {
            string path = "C:\\words.txt";
            StreamReader sr = new StreamReader(path);
            if (!File.Exists(path))
            {
                //Without creating a new file just I will throw an exception "FileNotFoundException"
               MessageBox.Show("Unfortunalely, your written txt file does not exist. Please write the fileName correctly:)");
            }
            else
            {
                try
                {
                    String line = sr.ReadLine();
                    while (line != null)
                    {
                        t.Insert(line);
                        line = sr.ReadLine();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    sr.Close();
                }
            }
            SpellCorrector.CreateDictionary("C:\\words.txt", "");
        }
    }
    
}
